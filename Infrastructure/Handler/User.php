<?php


namespace Infrastructure\Handler;

use Domain\Facade\Client as ClientFacade;
use Infrastructure\Service\RestorePassword\RestorePassword as RestorePasswordService;

class User
{
    private $restorePasswordService;
    private $clientFacade;

    public function __construct()
    {
        $this->restorePasswordService = new RestorePasswordService();
        $this->clientFacade = new ClientFacade();
    }

    public function initRestorePassword($email)
    {
        $user = $this->clientFacade->getUserByEmail($email);
        if ($user) {
            $token = $this->restorePasswordService->getRestoreToken($email);
            if ($token) {
                // Отправка письма с инструкцией по восстановлению пароля
                return true;
            } else {
                // Обработка ошибок
                return false;
            }
        }
        else {
            // Обработка ошибок - такой email не найден
            return false;
        }
    }

    public function setNewPassword($token, $password)
    {
        if ($this->restorePasswordService->isTokenValid($token)) {
            // TODO: Устанавливаем новый пароль
            $this->clientFacade->saveProfile();
        }
        else {
            // Обработка ошибок - не валидный токен
            return false;
        }
    }
}