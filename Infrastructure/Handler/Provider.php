<?php

namespace Infrastructure\Handler;

use Domain\Facade\Provider as ProviderFacade;

class Provider
{
    private $providerFacade;

    public function __construct()
    {
        $this->providerFacade = new ProviderFacade();
    }

    public function getAllOpenFreeDeals()
    {
        $deals = $this->providerFacade->getAllOpenFreeDeals();

        return $deals;
    }

    public function participateDeal($dealId, $providerId)
    {
        return $this->providerFacade->participateDeal();
    }

    public function cancelDeal($dealId, $providerId)
    {
        return $this->providerFacade->cancelDeal();
    }

    public function completeDeal($dealId)
    {
        return $this->providerFacade->completeDeal($dealId);
    }
}