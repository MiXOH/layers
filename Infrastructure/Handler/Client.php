<?php


namespace Infrastructure\Handler;

use Domain\Facade\Client as ClientFacade;

class Client
{
    private $clientFacade;

    public function __construct()
    {
        $this->clientFacade = new ClientFacade();
    }

    public function registryUser($email, $password)
    {
        return $this->clientFacade->registry($email, $password);
    }

    public function createOrder($clientId, $platformId, $coins)
    {
        $order = $this->clientFacade->createOrder();
    }

    public function createDeal($orderId, $countClientCoins)
    {
        $deal = $this->clientFacade->createDeal();
    }

    public function confirmDeal($dealId)
    {
        return $this->clientFacade->confirmDeal();
    }

    public function cancelOrder($orderId, $clientId)
    {
        $this->clientFacade->cancelOrder();
    }
}