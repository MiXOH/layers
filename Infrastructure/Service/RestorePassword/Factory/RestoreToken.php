<?php


namespace Domain\Model\Factory;

use Domain\Model\User as UserModel;
use Domain\Model\RestoreToken as RestoreTokenModel;

class RestoreToken
{
    public function create($user, $token, $expiryDate)
    {
        $restoreToken = new RestoreTokenModel();

        $restoreToken->setUser($user);
        $restoreToken->setToken($token);
        $restoreToken->setExpityDate($expiryDate);

        return $restoreToken;
    }
}