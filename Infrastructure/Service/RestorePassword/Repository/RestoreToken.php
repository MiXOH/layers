<?php


namespace Domain\Repository;

use Domain\Model\RestoreToken as RestoreTokenModel;

class RestoreToken
{
    public function save(RestoreTokenModel $restoreToken)
    {

    }

    /**
     * @param $token
     *
     * @return RestoreTokenModel
     */
    public function findOneAliveByToken($token)
    {
        // Возвращает "живую" запись по токену (т.е. ту у которой не истёк expiryDate)
    }
}