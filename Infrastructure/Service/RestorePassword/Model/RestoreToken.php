<?php


namespace Domain\Model;

use Domain\Model\User as UserModel;

class RestoreToken
{
    /**
     * @var User
     */
    private $user;

    private $token;
    private $expiryDate;

    public function setUser(UserModel $user)
    {
        $this->user = $user;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function setExpityDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    }

    public function getUser()
    {
        return $this->user;
    }

}