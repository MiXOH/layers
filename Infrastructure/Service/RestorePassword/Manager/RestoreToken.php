<?php


namespace Domain\Manager;

use Domain\Model\User as UserModel;
use Domain\Model\Factory\RestoreToken as RestoreTokenFactory;
use Domain\Repository\RestoreToken as RestoreTokenRepository;

class RestoreToken
{
    private $restoreTokenFactory;
    private $restoreTokenRepository;

    public function __construct()
    {
        $this->restoreTokenFactory = new RestoreTokenFactory();
        $this->restoreTokenRepository = new RestoreTokenRepository();
    }

    public function initTokenForUser(UserModel $user)
    {
        $token = $this->generateToken();
        $expiryDate = $this->getExpiryDate();

        $restoreToken = $this->restoreTokenFactory($user, $token, $expiryDate);
        $this->restoreTokenRepository->save($restoreToken);

        return $token;
    }

    public function getUserByToken($token)
    {
        $restoreToken = $this->restoreTokenRepository->findOneAliveByToken($token);
        if (!is_null($restoreToken)) {
            return $restoreToken->getUser();
        }
        else {
            // возвращаем ошибку, о том что токен не найден или истёк
            return false;
        }
    }

    protected function generateToken()
    {
        // Тут какой-то алгоритм генерации сложного и максимально уникального токена
        return rand();
    }

    protected function getExpiryDate()
    {
        // Тут можно тянуть время жизни токена из настроек
        return date('Y-m-d H:i:s');
    }
}