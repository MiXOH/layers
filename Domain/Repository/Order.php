<?php

namespace Domain\Repository;

use Domain\Model\Order as OrderModel;

class Order
{
    public function save(OrderModel $order)
    {

    }

    /**
     * @param $userId
     *
     * @return OrderModel
     */
    public function findOneByUserId($userId)
    {

    }

    /**
     * @param $id
     *
     * @return OrderModel
     */
    public function findOneById($id)
    {

    }

    /**
     * @param $orderId
     *
     * @return OrderModel
     */
    public function findOneByClientId($clientId)
    {

    }
}