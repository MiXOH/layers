<?php

namespace Domain\Repository;

use Domain\Model\User as UserModel;

class User
{
    public function save(User $user)
    {

    }

    public function findOne($email, $password)
    {

    }

    /**
     * @param $id
     *
     * @return UserModel
     */
    public function findOneById($id)
    {

    }

    /**
     * @param $email
     *
     * @return UserModel
     */
    public function findOneByEmail($email)
    {

    }
}