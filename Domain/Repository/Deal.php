<?php

namespace Domain\Repository;

use Domain\Model\Deal as DealModel;

class Deal
{
    public function save(DealModel $deal)
    {

    }

    /**
     * @param $id
     *
     * @return DealModel
     */
    public function findOneById($id)
    {

    }

    /**
     * @param $orderId
     *
     * @return DealModel
     */
    public function findOneByOrderId($orderId)
    {

    }

    /**
     * @param $providerId
     *
     * @return DealModel
     */
    public function findOneByProviderId($providerId)
    {

    }

    /**
     * Возвращает все активные сделки в которых не принимают участия поставщики
     *
     * @return Array of DealModel
     */
    public function findAllOpenFree()
    {

    }
}