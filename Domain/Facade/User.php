<?php


namespace Domain\Facade;

use Domain\Facade\DTO\User as UserDto;
use Domain\Facade\DTO\UserProfile as UserProfileDto;
use Domain\Manager\User as UserManager;
use Domain\Repository\User as UserRepository;

class User implements UserInterface
{
    private $userRepository;
    private $userManager;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->userManager = new UserManager();
    }

    public function registry($email, $password)
    {
        return $this->userManager->registry($email, $password);
    }

    public function login($email, $password)
    {
        $userDto = new UserDto();
        $user = $this->userManager->auth($email, $password);
        if ($user) {
            $userDto->id = $user->getId();
            $userDto->email = $user->getEmail();
            return $userDto;
        }
        else {
            // Обработка ошибки - пользователя с данными email и паролем нет в системе
            return false;
        }
    }

    public function saveProfile($userId, UserProfileDto $userProfile)
    {
        $user = $this->userRepository->findOneById($userId);
        if ($user) {
            $this->userManager->changeProfile(
                $user,
                ($userProfile->email) ? $userProfile->email : null,
                ($userProfile->password) ? $userProfile->password : null
            );
            return true;
        }
        else {
            // Обработка ошибки - пользователь не найден
            return false;
        }
    }

    public function getUserByEmail($email)
    {
        $user = $this->userRepository->findOneByEmail($email);
        if ($user) {
            $userDto = new UserDto();
            $userDto->id = $user->getId();
            $userDto->email = $user->getEmail();
            return $userDto;
        }
        else {
            // Обработка ошибки - пользователь не найден
            return false;
        }
    }
}