<?php


namespace Domain\Facade\DTO;


class DealClient
{
    public $id;
    public $playerName;
    public $playerPriceReal;
    public $playerPriceMax;
}