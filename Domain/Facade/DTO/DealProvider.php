<?php


namespace Domain\Facade\DTO;


class DealProvider
{
    public $id;
    public $playerName;
    public $playerPriceReal;
    public $playerPriceMax;
    public $countCoins;
}