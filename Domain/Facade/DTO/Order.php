<?php


namespace Domain\Facade\DTO;


class Order
{
    public $id;
    public $coinsOrdered;
    public $coinsDelivered;
}