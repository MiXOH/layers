<?php


namespace Domain\Facade;

use Domain\Model\Currency;
use Domain\Repository\User as UserRepository;
use Domain\Repository\Order as OrderRepository;
use Domain\Repository\Platform as PlatformRepository;
use Domain\Repository\ExchangeRate as ExchangeRateRepository;
use Domain\Manager\User as UserManager;
use Domain\Manager\Wallet as WalletManager;
use Domain\Manager\Order as OrderManager;
use Domain\Manager\Deal as DealManager;
use Domain\Facade\User as UserFacade;
use Domain\Facade\DTO\Order as OrderDto;
use Domain\Facade\DTO\DealClient as DealClientDto;

class Client extends UserFacade implements ClientInterface
{
    private $userRepository;
    private $platformRepository;
    private $orderRepository;
    private $exchangeRateRepository;
    private $userManager;
    private $walletManager;
    private $orderManager;
    private $dealManager;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->platformRepository = new PlatformRepository();
        $this->orderRepository = new OrderRepository();
        $this->exchangeRateRepository = new ExchangeRateRepository();
        $this->userManager = new UserManager();
        $this->walletManager = new WalletManager();
        $this->orderManager = new OrderManager();
        $this->dealManager = new DealManager();
    }

    public function depositMoney($clientId, $countMoney)
    {
        $client = $this->userRepository->findOneById($clientId);
        if ($client) {
            $this->walletManager->depositMoneyToCoinsWallet($client, $countMoney);
            return true;
        }
        else {
            // Обработка ошибки - пользователь не найден
            return false;
        }
    }
    public function getDepositMoneyHistory($clientId)
    {
        // TODO: Выяснить у бизнеса как должен работать метод
    }
    public function getMoneyCount($clientId)
    {
        $client = $this->userRepository->findOneById($clientId);
        if ($client) {
            return $this->walletManager->getCoinsAmount($client);
        }
        else {
            // Обработка ошибки - пользователь не найден
            return false;
        }
    }

    public function createOrder($clientId, $platformId, $coins)
    {
        $client = $this->userRepository->findOneById($clientId);
        $platform = $this->platformRepository->findOneById($platformId);

        if ($client) {
            if ($platform) {
                $order = $this->orderManager->create($client, $platform, $coins);

                $orderDto = new OrderDto();
                $orderDto->id = $order->getId();
                $orderDto->coinsOrdered = $order->getCoinsOrdered();
                $orderDto->coinsDelivered = $order->getCoinsDelivered();

                return $orderDto;
            }
            else {
                // Обработка ошибки - платформа не найдена
                return false;
            }
        }
        else {
            // Обработка ошибки - пользователь не найден
            return false;
        }
    }
    public function cancelOrder($orderId, $clientId)
    {
        $client = $this->userRepository->findOneById($clientId);
        $order = $this->orderRepository->findOneById($orderId);

        if ($client) {
            if ($order) {
                return $this->orderManager->cancel($order, $client);
            }
            else {
                // Обработка ошибки - заказ не найден
                return false;
            }
        }
        else {
            // Обработка ошибки - пользователь не найден
            return false;
        }
    }
    public function getCurrentOrder($clientId)
    {
        $order = $this->orderManager->getActive($clientId);
        if ($order) {
            $orderDto = new OrderDto();
            $orderDto->id = $order->getId();
            $orderDto->coinsOrdered = $order->getCoinsOrdered();
            $orderDto->coinsDelivered = $order->getCoinsDelivered();
            return $orderDto;
        }
        else {
            // Обработка ошибки - нет текущего заказа
            return false;
        }
    }
    public function getOrderHistory()
    {
        // TODO: Реализовать метод в самом конце
    }

    public function createDeal($orderId, $clientId, $curClientCoins)
    {
        $client = $this->userRepository->findOneById($clientId);
        $order = $this->orderRepository->findOneById($orderId);

        if ($client) {
            if ($order) {
                $deal = $this->dealManager->init($order, $client, $curClientCoins);

                $dealClientDto = new DealClientDto();
                $dealClientDto->id = $deal->getId();
                $dealClientDto->playerName = $deal->getPlayer()->getName();
                $dealClientDto->playerPriceMax = $deal->getPlayer()->getPriceMax();
                $dealClientDto->playerPriceReal = $deal->getPlayer()->getPriceReal();

                return $dealClientDto;
            }
            else {
                // Обработка ошибки - заказ не найден
                return false;
            }
        }
        else {
            // Обработка ошибки - пользователь не найден
            return false;
        }
    }
    public function confirmDeal($dealId)
    {
        return $this->dealManager->confirm($dealId);
    }

    public function getExchangeRate()
    {
        return $this->exchangeRateRepository->findOneByBuyAndSell(Currency::__COINS__, Currency::__RUB__);
    }
}