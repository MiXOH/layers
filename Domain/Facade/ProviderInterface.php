<?php


namespace Domain\Facade;

use Domain\Facade\DTO\DealProvider as DealProviderDto;

interface ProviderInterface
{
    /**
     * Получаем все доступные на данный момент сделки
     *
     * @return DealProviderDto Array
     */
    public function getAllOpenFreeDeals();

    /**
     * Принять участие в сделке
     *
     * @param $dealId
     * @param $providerId
     *
     * @return bool
     */
    public function participateDeal($dealId, $providerId);

    /**
     * Отменить участие в сделке
     *
     * @param $dealId
     * @param $providerId
     *
     * @return bool
     */
    public function cancelDeal($dealId, $providerId);

    /**
     * Завершить сделку
     *
     * @param $dealId
     *
     * @return bool
     */
    public function completeDeal($dealId);

    /**
     * Получение текущей сделки
     *
     * @param $providerId
     *
     * @return DealProviderDto|false
     */
    public function getCurrentDeal($providerId);

    // TODO: Реализовать метод
    public function getDealHistory();

    /**
     * Вывод денег со счёта
     *
     * @param $providerId
     * @param $countMoney
     *
     * @return bool
     */
    public function withdrawMoney($providerId, $countMoney);

    /**
     * Получаем текущее количество денег на счёте
     *
     * @param $providerId
     *
     * @return double|false
     */
    public function getMoneyCount($providerId);

    // TODO: Реализовать метод
    public function getWithdrawMoneyHistory();

    /**
     * Выводим текущий курс продажи монет
     *
     * @return double
     */
    public function getExchangeRate();
}