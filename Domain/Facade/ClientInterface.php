<?php


namespace Domain\Facade;

use Domain\Facade\DTO\Order as OrderDto;
use Domain\Facade\DTO\DealClient as DealDto;

interface ClientInterface
{
    /**
     * Пополнить кошелёк деньгами (монетами)
     *
     * @param $userId
     * @param $countMoney
     *
     * @return bool
     */
    public function depositMoney($clientId, $countMoney);

    // TODO: Реализовать метод
    public function getDepositMoneyHistory($clientId);

    /**
     * Получаем количество денег (монет) на счету пользователя
     *
     * @param $clientId
     *
     * @return double|false
     */
    public function getMoneyCount($clientId);

    /**
     * Создаём заказ
     *
     * @param $clientId
     * @param $platformId
     * @param $coins
     *
     * @return OrderDto|false
     */
    public function createOrder($clientId, $platformId, $coins);

    /**
     * Отменяем заказ
     *
     * @param $orderId
     * @param $clientId
     *
     * @return bool
     */
    public function cancelOrder($orderId, $clientId);

    /**
     * Возвращает текущий заказ
     *
     * @param $clientId
     *
     * @return OrderDto|false
     */
    public function getCurrentOrder($clientId);

    // TODO: Реализовать метод
    public function getOrderHistory();

    /**
     * Создаёт сделку
     *
     * @param $orderId
     * @param $clientId
     * @param $curClientCoins
     *
     * @return DealDto|false
     */
    public function createDeal($orderId, $clientId, $curClientCoins);

    /**
     * Подтверждение выполнения условий сделки (если успешно, то сделка выводится поставщикам)
     *
     * @param $dealId
     *
     * @return bool
     */
    public function confirmDeal($dealId);

    /**
     * Выводим текущий курс покупки монет
     *
     * @return double
     */
    public function getExchangeRate();
}