<?php


namespace Domain\Facade;

use Domain\Facade\DTO\User as UserDto;
use Domain\Facade\DTO\UserProfile as UserProfileDto;

interface UserInterface
{
    /**
     * Регистрация (создание) пользователя
     *
     * @param string $email
     * @param string $password
     *
     * @return bool
     */
    public function registry($email, $password);

    /**
     * Авторизация в системе, проверка наличия такого пользователя
     *
     * @param $email
     * @param $password
     *
     * @return UserDto|false
     */
    public function login($email, $password);

    /**
     * Сохраняет профайл (личные данные) пользователя
     *
     * @param $userId
     * @param UserProfileDto $userProfile
     *
     * @return bool
     */
    public function saveProfile($userId, UserProfileDto $userProfile);

    /**
     * Возвращает пользователя по его email
     *
     * @param $email
     *
     * @return UserDto|false
     */
    public function getUserByEmail($email);
}