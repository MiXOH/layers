<?php


namespace Domain\Facade;

use Domain\Model\Currency;
use Domain\Repository\ExchangeRate as ExchangeRateRepository;
use Domain\Repository\Deal as DealRepository;
use Domain\Repository\User as UserRepository;
use Domain\Manager\Deal as DealManager;
use Domain\Facade\User as UserFacade;
use Domain\Model\Deal as DealModel;
use Domain\Facade\DTO\DealProvider as DealProviderDto;
use Domain\Manager\Wallet as WalletManager;

class Provider extends UserFacade implements ProviderInterface
{
    private $exchangeRateRepository;
    private $dealRepository;
    private $userRepository;
    private $dealManager;
    private $walletManager;

    public function __construct()
    {
        $this->exchangeRateRepository = new ExchangeRateRepository();
        $this->dealRepository = new DealRepository();
        $this->userRepository = new UserRepository();
        $this->dealManager = new DealManager();
        $this->walletManager = new WalletManager();
    }

    public function getAllOpenFreeDeals()
    {
        $dealModelList = $this->dealManager->getAllOpenFree();

        $dealProviderDtoList = array();

        /**
         * @var int $key
         * @var DealModel $value
         */
        foreach ($dealModelList as $key => $value) {
            $dealProviderDto = new DealProviderDto();
            $dealProviderDto->id = $value->getId();
            $dealProviderDto->playerName = $value->getPlayer()->getName();
            $dealProviderDto->playerPriceReal = $value->getPlayer()->getPriceReal();
            $dealProviderDto->playerPriceMax = $value->getPlayer()->getPriceMax();
            $dealProviderDto->countCoins = $value->getCountCoins();

            $dealProviderDtoList[] = $dealProviderDto;
        }

        return $dealProviderDtoList;
    }
    public function participateDeal($dealId, $providerId)
    {
        $deal = $this->dealRepository->findOneById($dealId);
        $provider = $this->userRepository->findOneById($providerId);

        if ($provider) {
            if ($deal) {
                return $this->dealManager->participate($deal, $provider);
            }
            else {
                // Обработка ошибки - сделка не найдена
                return false;
            }
        }
        else {
            // Обработка ошибки - пользователь не найден
            return false;
        }
    }
    public function cancelDeal($dealId, $providerId)
    {
        $deal = $this->dealRepository->findOneById($dealId);
        $provider = $this->userRepository->findOneById($providerId);

        if ($provider) {
            if ($deal) {
                return $this->dealManager->cancel($deal, $provider);
            }
            else {
                // Обработка ошибки - сделка не найдена
                return false;
            }
        }
        else {
            // Обработка ошибки - пользователь не найден
            return false;
        }
    }
    public function completeDeal($dealId)
    {
        $deal = $this->dealRepository->findOneById($dealId);

        if ($deal) {
            return $this->dealManager->complete($deal);
        }
        else {
            // Обработка ошибки - сделка не найдена
            return false;
        }
    }
    public function getCurrentDeal($providerId)
    {
        $deal = $this->dealManager->getActive($providerId);

        if ($deal) {
            $dealProviderDto = new DealProviderDto();
            $dealProviderDto->id = $deal->getId();
            $dealProviderDto->playerName = $deal->getPlayer()->getName();
            $dealProviderDto->playerPriceReal = $deal->getPlayer()->getPriceReal();
            $dealProviderDto->playerPriceMax = $deal->getPlayer()->getPriceMax();
            $dealProviderDto->countCoins = $deal->getCountCoins();

            return $dealProviderDto;
        }
        else {
            // Обработка ошибки - нет текущей сделки
            return false;
        }
    }
    public function getDealHistory()
    {
        // TODO: Реализовать метод в самом конце
    }

    public function withdrawMoney($providerId, $countMoney)
    {
        $provider = $this->userRepository->findOneById($providerId);
        if ($provider) {
            $this->walletManager->withdrawMoney($provider, $countMoney);
            return true;
        }
        else {
            // Обработка ошибки - пользователь не найден
            return false;
        }
    }
    public function getMoneyCount($providerId)
    {
        $provider = $this->userRepository->findOneById($providerId);
        if ($provider) {
            return $this->walletManager->getMoneyAmount($provider);
        }
        else {
            // Обработка ошибки - пользователь не найден
            return false;
        }
    }
    public function getWithdrawMoneyHistory()
    {
        // TODO: Реализовать метод в самом конце
    }

    public function getExchangeRate()
    {
        return $this->exchangeRateRepository->findOneByBuyAndSell(Currency::__RUB__, Currency::__COINS__);
    }
}