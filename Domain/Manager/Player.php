<?php


namespace Domain\Manager;

use Domain\Model\Platform as PlatformModel;
use Domain\Model\Player as PlayerModel;
use Domain\Repository\Player as PlayerRepository;
use Domain\Service\TransferMarket as TransferMarketService;

class Player
{
    private $playerRepository;
    private $transferMarketService;

    public function __construct()
    {
        $this->playerRepository = new PlayerRepository();
        $this->transferMarketService = new TransferMarketService();
    }

    public function getPlayerForDeal(PlatformModel $platform, $curCoins, $needCoins)
    {
        $player = $this->playerRepository->findForDeal($platform, $curCoins, $needCoins);

        return $player;
    }

    public function findOnTransferMarket(PlayerModel $player)
    {
        // Проверяем наличие игрока на трансферном рынке
        // Связь с трансферным рынком должна быть организована через отдельный модуль (репозиторий или сервис)
        return $this->transferMarketService->findOne($player->getPriceReal(), $player->getPriceMax(), '');
    }
}