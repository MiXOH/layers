<?php

namespace Domain\Manager;

use Domain\Model\Deal as DealModel;
use Domain\Model\Order as OrderModel;
use Domain\Model\User as UserModel;
use Domain\Repository\Player as PlayerRepository;
use Domain\Repository\Deal as DealRepository;
use Domain\Repository\Order as OrderRepository;
use Domain\Repository\User as UserRepository;
use Domain\Manager\Player as PlayerManager;
use Domain\Manager\Wallet as WalletManager;
use Domain\Model\Factory\Deal as DealFactory;

class Deal
{
    private $playerRepository;
    private $dealRepository;
    private $orderRepository;
    private $userRepository;
    private $playerManager;
    private $walletManager;
    private $dealFactory;

    public function __construct()
    {
        $this->playerRepository = new PlayerRepository();
        $this->dealRepository = new DealRepository();
        $this->orderRepository = new OrderRepository();
        $this->userRepository = new UserRepository();
        $this->playerManager = new PlayerManager();
        $this->walletManager = new WalletManager();
        $this->dealFactory = new DealFactory();
    }

    public function init(OrderModel $order, UserModel $user, $curCoins)
    {
        $deal = $this->dealRepository->findOneByOrderId($order->getId());

        if ($order->getUser()->getId() == $user->getId()) {
            if (is_null($deal)) {
                // TODO: Вынести пороговое значение количества монет в настройки
                if ($curCoins > 1000) {
                    $orderNeedCoins = $order->getCoinsOrdered() - $order->getCoinsDelivered();

                    $needCoins = $this->getOptimizeDealSum($orderNeedCoins);

                    $player = $this->playerManager->getPlayerForDeal($order->getPlatform(), $curCoins, $needCoins);

                    $deal = $this->dealFactory->create($order, $needCoins, $player);

                    $this->dealRepository->save($deal);

                    return $deal;
                } else {
                    // Обработка ошибки - не хватает собственных монет клиента для проведения сделки
                    return false;
                }
            } else {
                // Обработка ошибки - активная сделка уже есть (может быть только одна активная сделка)
                return false;
            }
        }
        else {
            // Обработка ошибки - попытка создать сделку не по своему заказу
            return false;
        }
    }

    public function confirm($dealId)
    {
        $deal = $this->dealRepository->findOneById($dealId);

        if ($this->playerManager->findOnTransferMarket($deal->getPlayer())) {
            $deal->setStateReady();
            $this->dealRepository->save($deal);
            return true;
        }
        else {
            // Обработка ошибки - игрок не найден на трансферном рынке
            return false;
        }
    }

    public function cancel(DealModel $deal, UserModel $provider)
    {
        if ($deal->getProvider()->getId() == $provider->getId()) {
            $deal->clearProvider();
            $this->dealRepository->save($deal);
            return true;
        }
        else {
            // Обработка ошибки - сделка не привязана за данным пользователем
            return false;
        }
    }

    public function participate(DealModel $deal, UserModel $provider)
    {
        // Проверяем, что сделка свободна
        if (is_null($deal->getProvider())) {
            // Проверяем, у поставщика нет активных сделок
            if (is_null($this->getActive($provider->getId()))) {
                // TODO: Тут делаем транзакцию, чтобы сделку не взяли одновременно два поставщика
                $deal->setProvider($provider);
                $this->dealRepository->save($deal);
                return true;
            }
            else {
                // Обработка ошибки - активная сделка уже есть (может быть только одна активная сделка)
                return false;
            }
        }
        else {
            // Обработка ошибки - сделка уже взята другим поставщиком
            return false;
        }
    }

    public function complete($deal)
    {
        if (!$this->playerManager->findOnTransferMarket($deal->getPlayer())) {
            $amount = $deal->getCountCoins();
            $this->walletManager->depositCoinsToMoneyWallet($deal->getProvider(), $amount);

            $order = $deal->getOrder();
            $order->setCoinsDelivered($order->getCoinsDelivered() + $deal->getCountCoins());
            $this->orderRepository->save($order);

            $deal->setStateCompleted();
            $this->dealRepository->save($deal);
            return true;
        }
        else {
            // Обработка ошибки - игрок найден на трансферном рынке
            return false;
        }
    }

    /**
     * Получить все открытые свободные (не взятые поставщиками) сделки
     *
     * @return DealModel Array
     */
    public function getAllOpenFree()
    {
        $dealModels = $this->dealRepository->findAllOpenFree();

        return $dealModels;
    }

    public function getActive($providerId)
    {
        $deal = $this->dealRepository->findOneByProviderId($providerId);

        return $deal;
    }

    protected function getOptimizeDealSum($amount)
    {
        // TODO: Тут реализуется алгоритм дробления заказа, что-то надо будет подтянуть из настроек
        return $amount;
    }
}