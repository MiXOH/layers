<?php

namespace Domain\Manager;

use Domain\Repository\User as UserRepository;
use Domain\Manager\Wallet as WalletManager;
use Domain\Model\User as UserModel;
use Domain\Model\Factory\User as UserFactory;

class User
{
    private $userRepository;
    private $walletManager;
    private $userFactory;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->walletManager = new WalletManager();
        $this->userFactory = new UserFactory();
    }

    /**
     * @param $email
     * @param $password
     *
     * @return bool
     */
    public function registry($email, $password)
    {
        $user = $this->userRepository->findOneByEmail($email);
        if (!$user) {
            $user = $this->userFactory->create($email, $password);
            $this->userRepository->save($user);
            return true;
        }
        else {
            // Обработка ошибки - пользователь с данным email уже есть
            return false;
        }
    }

    /**
     * @param $email
     * @param $password
     *
     * @return UserModel
     */
    public function auth($email, $password)
    {
        return $this->userRepository->findOne($email, $password);
    }

    public function changeProfile(UserModel $user, $email = null, $password = null)
    {
        if (!is_null($email)) {
            $userWithNewEmail = $this->userRepository->findOneByEmail($email);
            if (!$userWithNewEmail) {
                $user->setEmail($email);
            } else {
                // Обработка ошибки - пользователь с данным email уже есть
                return false;
            }
        }

        if (!is_null($password)) {
            $user->setPassword($password);
        }

        $this->userRepository->save($user);
        return true;
    }

    public function withdrawMoney($id, $countMoney)
    {

    }

    public function reportDepositMoney($id, $criteria)
    {

    }

    public function reportWithdrawMoney($id, $criteria)
    {

    }
}