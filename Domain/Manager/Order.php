<?php

namespace Domain\Manager;

use Domain\Model\Factory\Order as OrderFactory;
use Domain\Model\User as UserModel;
use Domain\Model\Order as OrderModel;
use Domain\Model\Platform as PlatformModel;
use Domain\Manager\Wallet as WalletManager;
use Domain\Repository\User as UserRepository;
use Domain\Repository\Platform as PlatformRepository;
use Domain\Repository\Order as OrderRepository;
use Domain\Repository\Deal as DealRepository;

class Order
{
    private $orderFactory;
    private $userRepository;
    private $platformRepository;
    private $orderRepository;
    private $dealRepository;
    private $walletManager;

    public function __construct()
    {
        $this->orderFactory = new OrderFactory();
        $this->userRepository = new UserRepository();
        $this->platformRepository = new PlatformRepository();
        $this->orderRepository = new OrderRepository();
        $this->dealRepository = new DealRepository();
        $this->walletManager = new WalletManager();
    }

    public function create(UserModel $client, PlatformModel $platform, $coins)
    {
        $order = $this->orderRepository->findOneByUserId($client->getId());

        if (is_null($order)) {
            if ($this->walletManager->withdrawCoins($client, $coins)) {
                $order = $this->orderFactory->create($client, $platform, $coins);
                $this->orderRepository->save($order);

                return $order;
            }
            else {
                // Обработка ошибки о блокировке денег
                return false;
            }
        }
        else {
            // Обработка ошибки о наличии активного заказа
            return false;
        }
    }

    public function cancel(OrderModel $order, UserModel $client)
    {
        // Проверяем, чтобы клиент отменял только свой заказ
        if ($order->getUser()->getId() == $client->getId()) {
            if (!is_null($order->getDeal())) {
                $deal = $order->getDeal();
                if (is_null($deal->getProvider())) {
                    $deal->setStateCancelled();
                    $this->dealRepository->save($deal);
                }
                else {
                    // Обработка ошибки - сделка в обработке у поставщика
                    return false;
                }
            }
            $order->setStateCancelled();
            $this->orderRepository->save($order);

            // TODO: Тут должна быть транзакция, чтобы не отменили одновременно одну и туже сделку
            $amount = $order->getCoinsOrdered() - $order->getCoinsDelivered();
            $this->walletManager->depositCoins($order->getUser(), $amount);
            return true;
        }
        else {
            // Обработка ошибки - попытка отменить чужой заказ
            return false;
        }
    }

    public function getActive($clientId)
    {
        $order = $this->orderRepository->findOneByClientId($clientId);

        return $order;
    }
}