<?php


namespace Domain\Manager;


use Domain\Model\Currency;
use Domain\Model\Factory\Wallet as WalletFactory;
use Domain\Model\User as UserModel;
use Domain\Model\Wallet as WalletModel;
use Domain\Repository\Wallet as WalletRepository;
use Domain\Repository\ExchangeRate as ExchangeRateRepository;

class Wallet
{
    private $walletRepository;
    private $exchangeRateRepository;
    private $walletFactory;

    public function __construct()
    {
        $this->walletRepository = new WalletRepository();
        $this->walletFactory = new WalletFactory();
        $this->exchangeRateRepository = new ExchangeRateRepository();
    }

    public function getCoinsAmount(UserModel $user)
    {
        return $this->getWalletByCurrency($user, Currency::__COINS__)->getAmount();
    }

    public function getMoneyAmount(UserModel $user)
    {
        return $this->getWalletByCurrency($user, Currency::__RUB__)->getAmount();
    }

    public function depositMoneyToCoinsWallet(UserModel $user, $countMoney)
    {
        $exchangeRate = $this->exchangeRateRepository->findOneByBuyAndSell(Currency::__COINS__, Currency::__RUB__);
        $rate = $exchangeRate->getRate();
        $countCoins = $countMoney * $rate;
        $this->depositCoins($user, $countCoins);
    }

    public function depositCoinsToMoneyWallet(UserModel $user, $countCoins)
    {
        $exchangeRate = $this->exchangeRateRepository->findOneByBuyAndSell(Currency::__RUB__, Currency::__COINS__);
        $rate = $exchangeRate->getRate();
        $countMoney = $countCoins * $rate;
        $this->depositMoney($user, $countMoney);
    }

    /**
     * @param UserModel $user
     * @param $amount
     *
     * @return bool
     */
    public function depositCoins(UserModel $user, $amount)
    {
        // TODO: сделать тут транзакцию, чтобы нельзя было одновременно провести несколько переводов
        $wallet = $this->getWalletByCurrency($user, Currency::__COINS__);
        $wallet->setAmount($wallet->getAmount() + $amount);
        $this->walletRepository->save($wallet);
        return true;
    }

    /**
     * @param UserModel $user
     * @param $amount
     *
     * @return bool
     */
    public function depositMoney(UserModel $user, $amount)
    {
        // TODO: сделать тут транзакцию, чтобы нельзя было одновременно провести несколько переводов
        $wallet = $this->getWalletByCurrency($user, Currency::__RUB__);
        $wallet->setAmount($wallet->getAmount() + $amount);
        $this->walletRepository->save($wallet);
        return true;
    }

    /**
     * @param $amount
     *
     * @return bool
     */
    public function withdrawCoins(UserModel $user, $amount)
    {
        // TODO: сделать тут транзакцию, чтобы нельзя было одновременно провести несколько переводов
        $wallet = $this->getWalletByCurrency($user, Currency::__COINS__);
        if ($wallet->getAmount() >= $amount) {
            $wallet->setAmount($wallet->getAmount() - $amount);
            $this->walletRepository->save($wallet);
            return true;
        }
        else {
            // Возвращаем ошибку о нехватке денег
            return false;
        }
    }

    public function withdrawMoney(UserModel $user, $amount)
    {
        // TODO: сделать тут транзакцию, чтобы нельзя было одновременно провести несколько переводов
        $wallet = $this->getWalletByCurrency($user, Currency::__RUB__);
        if ($wallet->getAmount() >= $amount) {
            $wallet->setAmount($wallet->getAmount() - $amount);
            $this->walletRepository->save($wallet);
            return true;
        }
        else {
            // Возвращаем ошибку о нехватке денег
            return false;
        }
    }

    protected function getWalletByCurrency(UserModel $user, $currency)
    {
        $wallet = null;
        /**
         * @var int $key
         * @var WalletModel $value
         */
        foreach ($user->getWallets() as $key => $value) {
            if ($value->getCurrency() == $currency) {
                $wallet = $value;
                break;
            }
        }

        if (is_null($wallet)) {
            $wallet = $this->walletFactory->create($user, $currency);
            $this->walletRepository->save($wallet);
            $user->addWallet($wallet);
        }

        return $wallet;
    }

}