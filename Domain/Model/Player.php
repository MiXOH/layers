<?php

namespace Domain\Model;

use Domain\Model\Platform as PlatformModel;

class Player
{
    private $id;
    private $name;
    private $type;

    /**
     * @var PlatformModel
     */
    private $platform;

    private $priceMax;
    private $priceReal;

    public function getName()
    {
        return $this->name;
    }

    public function getPriceMax()
    {
        return $this->priceMax;
    }

    public function getPriceReal()
    {
        return $this->priceReal;
    }
}