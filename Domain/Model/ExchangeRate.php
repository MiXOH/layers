<?php


namespace Domain\Model;


class ExchangeRate
{
    private $buyCurrency;
    private $sellCurrency;
    private $rate;

    public function getBuyCurrency()
    {
        return $this->buyCurrency;
    }

    public function getSellCurrency()
    {
        return $this->sellCurrency;
    }

    public function getRate()
    {
        return $this->rate;
    }
}