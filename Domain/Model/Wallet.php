<?php

namespace Domain\Model;

use Domain\Model\User as UserModel;

class Wallet
{
    private $id;

    /**
     * @var UserModel
     */
    private $user;

    private $currency;
    private $amount = 0;

    public function getAmount()
    {
        return $this->amount;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setUser(UserModel $user)
    {
        $this->user = $user;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }
}