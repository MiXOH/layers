<?php

namespace Domain\Model;

class OrderState
{
    // Заказ открыт
    const __OPEN__ = '__OPEN__';

    // Заказ отменён
    const __CANCELLED__ = '__CANCELLED__';

    // Заказ выполнен (завершён)
    const __COMPLETED__ = '__COMPLETED__';
}