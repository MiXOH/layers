<?php

namespace Domain\Model\Factory;


use Domain\Model\User as UserModel;
use Domain\Model\Wallet as WalletModel;

class Wallet
{
    public function create(UserModel $user, $currency, $amount = 0)
    {
        $wallet = new WalletModel();

        $wallet->setUser($user);
        $wallet->setCurrency($currency);
        $wallet->setAmount($amount);

        return $wallet;
    }
}