<?php

namespace Domain\Model\Factory;

use Domain\Model\Order as OrderModel;
use Domain\Model\Platform as PlatformModel;
use Domain\Model\User as UserModel;

class Order
{
    /**
     * @param UserModel $client
     * @param PlatformModel $platform
     * @param $coins
     *
     * @return OrderModel
     */
    public function create(UserModel $client, PlatformModel $platform, $coins)
    {
        $order = new OrderModel();

        $order->setUser($client);
        $order->setPlatform($platform);
        $order->setCoinsOrdered($coins);
        $order->setCoinsDelivered(0);
        $order->setStateOpen();

        return $order;
    }
}