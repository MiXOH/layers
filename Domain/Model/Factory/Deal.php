<?php


namespace Domain\Model\Factory;

use Domain\Model\Order as OrderModel;
use Domain\Model\Player as PlayerModel;
use Domain\Model\Deal as DealModel;

class Deal
{
    public function create(OrderModel $order, $needCoins, PlayerModel $player)
    {
        $deal = new DealModel();

        $deal->setOrder($order);
        $deal->setCountCoins($needCoins);
        $deal->setPlayer($player);
        $deal->setStateOpen();

        return $deal;
    }
}