<?php


namespace Domain\Model\Factory;

use Domain\Model\User as UserModel;

class User
{
    public function create($email, $password)
    {
        $user = new UserModel();

        $user->setEmail($email);
        $user->setPassword($password);

        return $user;
    }
}