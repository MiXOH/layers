<?php

namespace Domain\Model;

use Domain\Model\Player as PlayerModel;
use Domain\Model\Order as OrderModel;
use Domain\Model\User as UserModel;

class Deal
{
    private $id;

    /**
     * @var UserModel
     */
    private $provider;

    private $countCoins;

    // Зависимость на модель DealState
    private $state;

    /**
     * @var PlayerModel
     */
    private $player;

    /**
     * @var OrderModel
     */
    private $order;

    private $created;

    public function getId()
    {
        return $this->id;
    }

    public function getCountCoins()
    {
        return $this->countCoins;
    }

    /**
     * @return OrderModel
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return PlayerModel
     */
    public function getPlayer()
    {
        return $this->player;
    }

    /**
     * @return UserModel
     */
    public function getProvider()
    {
        return $this->provider;
    }

    public function clearProvider()
    {
        $this->setProvider(null);
    }

    public function setCountCoins($countCoins)
    {
        $this->countCoins = $countCoins;
    }

    public function setPlayer(PlayerModel $player)
    {
        $this->player = $player;
    }

    public function setOrder(OrderModel $order)
    {
        $this->order = $order;
    }

    public function setProvider(UserModel $provider)
    {
        $this->provider = $provider;
    }

    public function setState($state)
    {
        $this->state = $state;
    }

    public function setStateOpen()
    {
        $this->setState(DealState::__OPEN__);
    }

    public function setStateReady()
    {
        $this->setState(DealState::__READY__);
    }

    public function setStateCancelled()
    {
        $this->setState(DealState::__CANCELLED__);
    }

    public function setStateCompleted()
    {
        $this->setState(DealState::__COMPLETED__);
    }
}