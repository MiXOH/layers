<?php


namespace Domain\Model;

// Пока не ясно как лучше организовать работу с валютами

class Currency
{
    const __COINS__ = 'COINS';
    const __RUB__ = 'RUB';
    const __USD__ = 'USD';

    public function getCoins()
    {
        return self::__COINS__;
    }

    public function getRUB()
    {
        return self::__RUB__;
    }

    public function getUSD()
    {
        return self::__USD__;
    }
}