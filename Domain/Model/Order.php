<?php

namespace Domain\Model;

class Order
{
    private $id;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Platform
     */
    private $platform;
    private $coinsOrdered;
    private $coinsDelivered;
    private $created;

    // Зависимость на модель OrderState
    private $state;

    /**
     * @var Deal
     */
    private $deal;

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function setPlatform(Platform $platform)
    {
        $this->platform = $platform;
    }

    public function setCoinsOrdered($coins)
    {
        $this->coinsOrdered = $coins;
    }

    public function setCoinsDelivered($coins)
    {
        $this->coinsDelivered = $coins;
    }

    public function setState($state)
    {
        $this->state = $state;
    }

    public function setStateOpen()
    {
        $this->setState(OrderState::__OPEN__);
    }

    public function setStateCancelled()
    {
        $this->setState(OrderState::__CANCELLED__);
    }

    public function setStateCompleted()
    {
        $this->setState(OrderState::__COMPLETED__);
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Deal
     */
    public function getDeal()
    {

    }

    public function getCoinsOrdered()
    {
        return $this->coinsOrdered;
    }

    public function getCoinsDelivered()
    {
        return $this->coinsDelivered;
    }

    public function getPlatform()
    {
        return $this->platform;
    }

    public function getUser()
    {
        return $this->user;
    }

}