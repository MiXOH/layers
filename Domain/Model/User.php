<?php

namespace Domain\Model;

use Domain\Model\Factory\Wallet as WalletFactory;
use Domain\Model\Wallet as WalletModel;

class User
{
    private $id;
    private $email;
    private $password;

    /**
     * @var array of Wallet
     */
    private $wallets;

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getWallets()
    {
        return $this->wallets;
    }

    public function addWallet(WalletModel $wallet)
    {
        $this->wallets[] = $wallet;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
}