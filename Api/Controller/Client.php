<?php


namespace Api\Controller;

use Infrastructure\Handler\Client as ClientHandler;
use Api\Controller\User as UserController;

class Client extends UserController
{
    private $clientHandler;

    public function __construct()
    {
        $this->clientHandler = new ClientHandler();
    }

    public function createOrderAction($clientId, $platformId, $coins)
    {
        $order = $this->clientHandler->createOrder($clientId, $platformId, $coins);
    }

    public function createDealAction($orderId, $countClientCoins)
    {
        $deal = $this->clientHandler->createDeal($orderId, $countClientCoins);
    }

    public function cancelOrderAction($orderId, $clientId)
    {
        $this->clientHandler->cancelOrder($orderId, $clientId);
    }

    public function confirmDealAction($dealId)
    {
        $this->clientHandler->confirmDeal($dealId);
    }
}