<?php


namespace Api\Controller;

use Infrastructure\Handler\User as UserHandler;

abstract class User
{
    private $userHandler;

    public function __construct()
    {
        $this->userHandler = new UserHandler();
    }

    public function initRestorePasswordAction($email)
    {
        return $this->userHandler->initRestorePassword($email);
    }

    public function setNewPassword($token, $password)
    {
        return $this->userHandler->setNewPassword($token, $password);
    }
}