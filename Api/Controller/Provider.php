<?php


namespace Api\Controller;

use Infrastructure\Handler\Provider as ProviderHandler;
use Api\Controller\User as UserController;

class Provider extends UserController
{
    private $providerHandler;

    public function __construct()
    {
        $this->providerHandler = new ProviderHandler();
    }

    public function getAllOpenFreeDealsAction()
    {
        $deals = $this->providerHandler->getAllOpenFreeDeals();
    }

    public function participateDealAction($dealId, $providerId)
    {
        $this->providerHandler->participateDeal($dealId, $providerId);
    }

    public function cancelDealAction($dealId, $providerId)
    {
        $this->providerHandler->cancelDeal($dealId, $providerId);
    }

    public function completeDealAction($dealId)
    {
        $this->providerHandler->completeDeal($dealId);
    }
}